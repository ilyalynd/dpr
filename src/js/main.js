'use strict'

// Swiper
import Swiper, { Navigation, Autoplay } from 'swiper';

// Three.js
import * as THREE from 'three';
import { STLLoader } from 'three/examples/jsm/loaders/STLLoader';


(function () {
  const html = document.querySelector('html');


  // Добавляет слайдер с баннерами
  const banner = new Swiper('.banner', {
    slidesPerView: 1,
    loop: true,
    speed: 3000,

    modules: [ Autoplay ],
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    }
  });


  // Добавляет слайдер с событиями
  const events = new Swiper('.events__main', {
    slidesPerView: 2,
    spaceBetween: 16,
    autoHeight: true,
    loop: true,

    // Навигация
    modules: [ Navigation ],
    navigation: {
      prevEl: '.events-prev',
      nextEl: '.events-next'
    },

    breakpoints: {
      768: {
        // setWrapperSize: true,
        slidesPerView: 3
      },
      1216: {
        slidesPerView: 3,
        spaceBetween: 48
      }
    }
  });


  // Добавляет слайдер с отзывами
  const reviews = new Swiper('.reviews', {
    slidesPerView: 2,
    spaceBetween: 24,
    autoHeight: true,
    loop: true,

    breakpoints: {
      768: {
        slidesPerView: 3
      },
      1216: {
        slidesPerView: 3,
        spaceBetween: 100
      }
    }
  });


  // Добавляет слайдеры с последними темами
  const themes = new Swiper('.themes__main', {
    slidesPerView: 2,
    spaceBetween: 16,
    autoHeight: true,
    loop: true,

    // Навигация
    modules: [ Navigation ],
    navigation: {
      prevEl: '.themes-prev',
      nextEl: '.themes-next'
    },

    breakpoints: {
      768: {
        slidesPerView: 3
      },
      1216: {
        slidesPerView: 3,
        spaceBetween: 56
      }
    }
  });

  const lastThemes = new Swiper('.last-themes', {
    slidesPerView: 2,
    spaceBetween: 16,
    autoHeight: true,
    loop: true,

    breakpoints: {
      768: {
        slidesPerView: 3
      },
      1216: {
        slidesPerView: 3,
        spaceBetween: 56
      }
    }
  });


  // Добавляет слайдер с партнерами
  const partners = new Swiper('.partners', {
    slidesPerView: 3,
    spaceBetween: 24,
    loop: true,
    speed: 5000,

    modules: [ Autoplay ],
    autoplay: {
      delay: 1,
      disableOnInteraction: false
    },

    breakpoints: {
      768: {
        slidesPerView: 4
      },
      1216: {
        slidesPerView: 5,
        spaceBetween: 48,
        autoHeight: true
      }
    }
  });


  // Открывает / закрывает меню
  let linkMenu = document.querySelectorAll('[data-action=toggle-menu]'),
      linkOpenSubmenu = document.querySelectorAll('[data-action=open-submenu]'),
      linkCloseSubmenu = document.querySelectorAll('[data-action=close-submenu]');

  function toggleMenu(event) {
    event.preventDefault();

    let menu = document.querySelector('.menu'),
        link = document.querySelector('.header-menu-bar a');

    // Плавная прокрутка к началу страницы, чтобы выровнять меню
    window.scroll({ top: 0, behavior: 'smooth' })

    if (menu.classList.contains('menu--open')) {
      link.setAttribute('title', 'Открыть меню');

      html.style.overflowY = 'visible';
      menu.classList.remove('menu--open');
    }

    else {
      link.setAttribute('title', 'Закрыть меню');

      menu.classList.add('menu--open');
      html.style.overflowY = 'hidden';
    }
  }

  function openSubmenu(event) {
    event.preventDefault();

    let menu = document.querySelector('.main-menu'),
        submenu = document.querySelector('.main-submenu');

    menu.style.display = 'none';
    submenu.style.display = 'block';
  }

  function closeSubmenu(event) {
    event.preventDefault();

    let menu = document.querySelector('.main-menu'),
        submenu = document.querySelector('.main-submenu');

    submenu.style.display = 'none';
    menu.style.display = 'block';
  }

  linkMenu.forEach(link => link.addEventListener('click', toggleMenu));
  linkOpenSubmenu.forEach(link => link.addEventListener('click', openSubmenu));
  linkCloseSubmenu.forEach(link => link.addEventListener('click', closeSubmenu));


  // 3D логотип
  // var container, camera, scene, renderer;
  // var x = 1, y = 1, z = 1;

  // init();
  // animate();

  // function init() {
  //   container = document.querySelector('.movement__shape');

  //   // Renderer
  //   renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
  //   renderer.setSize(container.offsetWidth, container.offsetHeight);
  //   container.appendChild(renderer.domElement);

  //   // Scene
  //   scene = new THREE.Scene();

  //   // Camera
  //   camera = new THREE.PerspectiveCamera(0, container.offsetWidth / container.offsetHeight, 1, 10000);
  //   camera.position.set(x, y, z);
  //   scene.add(camera); // требуется, потому что мы добавляем источник света в качестве дочернего элемента камеры

  //   // Lights
  //   scene.add(new THREE.AmbientLight(0x222222));

  //   let light = new THREE.PointLight(0xffffff, 0.8);
  //   camera.add(light);

  //   // Object
  //   let loader = new STLLoader();

  //   loader.load('/model/logo3d.stl', function (geometry) {
  //     let material = new THREE.MeshPhongMaterial({ color: 0xff5533 });
  //     let mesh = new THREE.Mesh(geometry, material);

  //     scene.add(mesh);
  //   });

  //   window.addEventListener('resize', onWindowResize, false);
  // }

  // function onWindowResize() {
  //   camera.aspect = container.innerWidth / container.innerHeight;

  //   camera.updateProjectionMatrix();
  //   renderer.setSize(container.innerWidth, container.innerHeight);
  // }

  // function animate() {
  //   requestAnimationFrame(animate);
  //   render();

  //   if (window.pageYOffset < 768) {
  //     window.addEventListener('scroll', function (event) {
  //       x = event.clientX / window.innerWidth,
  //       y = event.clientY / window.innerHeight;

  //       render();
  //     });
  //   }

  //   else {
  //     window.addEventListener('mousemove', function(event) {
  //       x = event.clientX / window.innerWidth,
  //       y = event.clientY / window.innerHeight;

  //       render();
  //     });
  //   }
  // }

  // function render() {
  //   camera.position.x = Math.cos(x) * 2;
  //   camera.position.y = Math.sin(y) * 2;

  //   camera.lookAt(scene.position);
  //   renderer.render(scene, camera);
  // }


  // Добавляет анимации
  let objectWrappers = document.querySelectorAll('.object-wrapper');

  objectWrappers.forEach(objectWrapper => {
    let objects = objectWrapper.querySelectorAll('.object');

    objects.forEach(object => {
      object.classList.remove('object-animation');

      let observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
          if (typeof getCurrentAnimationPreference === 'function' && !getCurrentAnimationPreference()) {
            return;
          }

          if (entry.isIntersecting) {
            object.classList.add('object-animation');
            return;
          }

          object.classList.remove('object-animation');
        });
      });

      observer.observe(objectWrapper);
    });
  });
})();
