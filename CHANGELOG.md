# DPR change log

## Version 1.0.0 under development

- New: Added templates for all pages
- New: Added Bebas Neue and Bitter fonts
- New: Added Three.js
- New: Added Swiper
- New: Added Pug, SCSS, PostCSS
- New: Added Webpack, Babel

## Project init January 31, 2023
